extends Node2D

export var spacing = 80
export var item_offset = Vector2(50,0)

var plainsscene = preload("res://Plains/Plains.tscn")
var riverscene = preload("res://River/River.tscn")
var desertscene = preload("res://Desert/Desert.tscn")
var landscape_scenes = [plainsscene, riverscene, desertscene]
var chestscene = preload("res://Chest/Chest.tscn")

var turncounter = 0

signal shift_done
signal activated_all_cardspaces

func _ready():
	# center horizontally in viewport
	global_position.x = get_viewport_rect().get_center().x

	# spawn three placeholder cards
	for i in [1, 0, -1]:
		# make a turn node
		var turn = Node2D.new()
		
		if i == -1:
			# make a chest
			var chest = chestscene.instance()
			chest.position += item_offset
			turn.add_child(chest)
		
		# make a landscape card
		var card = random_landscape_scene().instance()
		turn.add_child(card)
		
		# place turn
		turn.position.y = spacing * i
		add_child(turn)

func get_turn_landscape(turn):
	for c in turn.get_children():
		if c.is_in_group("landscapes"):
			return c
	return null

func get_turn_event(turn):
	for c in turn.get_children():
		if c.is_in_group("events"):
			return c
	return null

func random_landscape_scene():
	return landscape_scenes[randi() % len(landscape_scenes)]

func add_new_turn():
	# adds a new turn just outside the playing field
	var turn = Node2D.new()
	turn.position.y = spacing * -2
	
	# add chest every 5th turn
	var safe_turncounter = float(turncounter + 1)

	var chest_chance = 0.33 * 20.0 * (1.0 / (20.0 + safe_turncounter))
	var dice_roll = float(randi() % 1000) / 1000.0
	
	print(chest_chance)
	print(dice_roll)
	print(1.0 / 3.0)
	
	if dice_roll < chest_chance:
		var chest = chestscene.instance()
		chest.position += item_offset
		turn.add_child(chest)

	# add landscape every turn
	var landscape = random_landscape_scene().instance()
	turn.add_child(landscape)

	add_child(turn)

func activate_cards():
	var current_turn = get_child(0)
	# let the cards in play know
	var cardspaces = get_tree().get_nodes_in_group("cardspaces")
	var this_landscape_card = get_turn_landscape(current_turn)
	for cs in cardspaces:
		print("cs")
		cs.activate_landscape(this_landscape_card)
		yield(cs, "done_activating")
	emit_signal("activated_all_cardspaces")
	print("done activating all cardspaces")

func shift_forward():
	turncounter += 1
	get_node("../TurnCounter").text = str(turncounter)
	
	activate_cards()
	yield(self, "activated_all_cardspaces")

	add_new_turn()
	var turns = get_children()
	var upcoming_turns = turns.slice(1, len(turns)-1)
	var current_turn = turns[0]

	# move current turn cards left
	for card in current_turn.get_children():
		card.move(
			Vector2(-200, card.global_position.y),
			0.5, # keep number low to appease race condition o_o
			Tween.TRANS_EXPO
		)
		# kill card when tween is done
		card.get_node("Tween").connect("tween_all_completed", card, "queue_free")

	# wait for landscape card tween to complete
	yield(get_tree().create_timer(1), "timeout")
	
	# move upcoming cards down
	for turn in upcoming_turns:
		yield(get_tree().create_timer(0.1), "timeout")
		for card in turn.get_children():
			card.move(
				Vector2(card.global_position.x, card.global_position.y + spacing),
				1,
				Tween.TRANS_EXPO
			)

	# kill turn node
	current_turn.queue_free()

	# you've activated my trap card!
	# trigger event cards
	var current_event_card = get_turn_event(upcoming_turns[0])
	if current_event_card != null:
		current_event_card.emit_signal("activate_event")
		yield(current_event_card, "event_done")

	emit_signal("shift_done")
