extends Node

signal dialog_done

onready var world = get_tree().root.get_node("World")
onready var discard = world.get_node("Discard")
var bowscene = preload("res://Bow/Bow.tscn")

func set_left_option(card):
	add_option(card, $LeftCard)

func set_right_option(card):
	add_option(card, $RightCard)

func add_option(card, destination):
	destination.add_child(card)
	card.z_index = 4010
	destination.move_child(card, 0)
	card.set_rotation(0)

func is_click(event):
	return event is InputEventMouseButton and event.button_index == BUTTON_LEFT and not event.pressed

func left_clicked(event):
	if is_click(event):
		card_chosen($LeftCard)

func right_clicked(event):
	if is_click(event):
		card_chosen($RightCard)

func find_card_in(container):
	for c in container.get_children():
		if c.is_in_group("cards"):
			return c
	return null

func card_chosen(container):
	# get the card and send it
	var card = find_card_in(container)
	card.to_zone(discard)
	card.make_top_card()
	
	# hide other stuff
	self.visible = false
	
	# wait for card to arrive
	yield(card.get_node("Tween"), "tween_all_completed")
	emit_signal("dialog_done")
	queue_free()

func _ready():
	# DEBUG just placeholder cards
	#set_left_option(bowscene.instance())
	#set_right_option(bowscene.instance())
	
	$LeftCard/Clickable.connect("gui_input", self, "left_clicked")
	$RightCard/Clickable.connect("gui_input", self, "right_clicked")
