extends Node

onready var root = get_tree().root.get_node("World")
onready var opponent = root.get_node("Opponent")
onready var discard = root.get_node("Discard")
onready var hand = root.get_node("Hand")
onready var food = root.get_node("Food")


func _ready():
	yield(get_parent(), "ready")
	var parent = get_parent()
	parent.set_durability(2)
	parent.set_texture("res://assets/hammer.png")
	parent.connect("activate_landscape", self, "_on_landscape")
	parent.get_node("Durability").text = str(parent.durability)
	parent.get_node("Name").text = "HAMMER"
	parent.get_node("Description").text = "REPAIRS\nNEIGH-\nBORS"
	
	

func _on_landscape(card):
	print("hammer triggered")
	# check if card is in play
	if not get_parent().is_in_play():
		print("not in play")
		return # do nothing
		
	var card_spaces = root.get_card_spaces()
	
	var own_index = null
	for i in range(len(card_spaces)):
		if card_spaces[i] == get_parent().get_parent():
			own_index = i
			
	var targets = []
	if own_index > 0:
		targets.append(own_index - 1)
	if own_index < (len(card_spaces) - 1):
		targets.append(own_index + 1)
	
	for i in targets:
		var targetcard = card_spaces[i].get_card()
		if targetcard != null:
			targetcard.increment_durability()

	get_parent().decrement_durability()
