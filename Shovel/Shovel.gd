extends Node

onready var bowscene = preload("res://Bow/Bow.tscn")
onready var chestdialogscene = preload("res://ChestDialog/ChestDialog.tscn")
onready var fishingrodscene = preload("res://FishingRod/FishingRod.tscn")
onready var hammerscene = preload("res://Hammer/Hammer.tscn")
onready var possible_scenes = [bowscene, bowscene, fishingrodscene, fishingrodscene, hammerscene]

onready var root = get_tree().root.get_node("World")

func _ready():
	yield(get_parent(), "ready")
	var parent = get_parent()
	parent.set_durability(1)
	parent.set_texture("res://assets/spade.png")
	parent.connect("activate_landscape", self, "_on_landscape")
	parent.get_node("Name").text = "SHOVEL"
	parent.get_node("Description").text = "SEARCH\nSAND"

func _on_landscape(card):

	if not get_parent().is_in_play():
		return # do nothing

	if card.has_sand:
		var dialog = chestdialogscene.instance()
		var choice1 = randi() % len(possible_scenes)
		var choice2 = randi() % len(possible_scenes)

		#make sure the two choices are distinct
		while possible_scenes[choice1] == possible_scenes[choice2]:
			choice2 = randi() % len(possible_scenes)

		dialog.set_left_option(possible_scenes[choice1].instance())
		dialog.set_right_option(possible_scenes[choice2].instance())

		root.add_child(dialog)
		yield(dialog, "dialog_done")
		get_parent().emit_signal("event_done")

	get_parent().decrement_durability()
