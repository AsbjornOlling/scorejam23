extends Node2D

# these signals are generally used by card variants
signal played
signal activate_landscape(card)
signal activate_event
signal event_done

export var is_draggable = true

onready var root = get_tree().root.get_child(0)
onready var deck = root.get_node("Deck")
onready var discard = root.get_node("Discard")
onready var hand = root.get_node("Hand")
onready var hand_area = root.get_node("HandArea")
onready var hand_area_coordinates = [hand_area.get_node("top_left"), hand_area.get_node("bottom_right")]
onready var food = root.get_node("Food")
onready var walk = root.get_node("Walk")

var random = RandomNumberGenerator.new()
onready var dragging = false

# environment card attributes
onready var has_game = false
onready var has_water = false
onready var has_trees = false
onready var has_sand = false

onready var durability = null

func set_durability(dur):
	durability = dur
	$Durability.text = str(durability)
	
func decrement_durability():
	durability -= 1
	$Durability.text = str(durability)
	if durability < 1:
		move(
			Vector2(self.global_position.x, -400),
			1, # keep number low to appease race condition o_o
			Tween.TRANS_EXPO
		)
		# kill card when tween is done
		get_node("Tween").connect("tween_all_completed", self, "queue_free")

func increment_durability():
	durability += 1
	$Durability.text = str(durability)


func _physics_process(delta):
	if dragging and is_draggable:
		var mousepos = get_viewport().get_mouse_position()
		self.move(mousepos)

func move(to: Vector2, time=0.1, interp=Tween.TRANS_LINEAR):
	var rounded_to = Vector2(floor(to.x), floor(to.y))
	$Tween.interpolate_method(
		self,
		"set_global_position",
		self.global_position,
		rounded_to,
		time,
		interp,
		Tween.EASE_OUT
	)
	$Tween.start()

func set_global_position(pos: Vector2):
	self.global_position = Vector2(floor(pos.x), floor(pos.y))

func small_rotation():
	self.rotation = 0
	self.rotate((randi() % 100) * PI/2000 - PI/40 )

func small_translation_vector(max_distance):
	var offset_x = randi() % (2 * max_distance) - max_distance
	var offset_y = randi() % (2 * max_distance) - max_distance
	return Vector2(offset_x, offset_y)

func to_zone(target_zone):
	$Highlight.visible = false
	
	#make node a child of target_node, and prevent global position from updating
	var global_position_before_move = self.global_position
	self.get_parent().remove_child(self)
	target_zone.add_child(self)
	self.set_owner(target_zone)
	self.global_position = global_position_before_move

	if target_zone == deck:
		move(deck.position + small_translation_vector(5), 0.3)
		small_rotation()
		self.is_draggable = false
		face_down()
	if target_zone == discard:
		move(discard.position + small_translation_vector(10), 0.3)
		small_rotation()
		self.is_draggable = false
	if target_zone == hand:
		var minx
		var miny
		var maxx
		var maxy
		minx = hand_area_coordinates[0].position.x
		maxx = hand_area_coordinates[1].position.x
		miny = hand_area_coordinates[0].position.y
		maxy = hand_area_coordinates[1].position.y
		self.is_draggable = true

		var position_x = (randi() % int(maxx-minx)) + minx
		var position_y = (randi() % int(maxy-miny)) + miny

		move(Vector2(position_x, position_y), 0.3)
		small_rotation()
		face_up()
	if target_zone == food:
		move(food.position + small_translation_vector(10), 0.3)
		small_rotation()
		self.is_draggable = false
	if target_zone == walk:
		move(walk.position + small_translation_vector(5), 0.3)
		small_rotation()
		self.is_draggable = true
	if target_zone.is_in_group("cardspaces"):
		small_rotation()
		move(target_zone.position)
		self.is_draggable = true

func make_top_card():
	var maxz = 0
	for card in get_tree().get_nodes_in_group("cards"):
		if card.z_index > 4000:
			# special cards. skip these
			continue
		if (card != self) and (card.z_index > maxz):
			maxz = card.z_index
	
	# reset all cards if we're reaching 4k
	if maxz == 3999:
		for card in get_tree().get_nodes_in_group("cards"):
			card.z_index = 10
		self.z_index += 1
		return
	
	self.z_index = maxz + 1

func is_in_play():
	return get_parent().is_in_group("cardspaces")

func _on_Control_gui_input(event):
	var parent = self.get_parent()
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			if is_draggable:
				dragging = true
				make_top_card()

		elif event.button_index == BUTTON_LEFT and !event.pressed:
			if dragging:
				dragging = false
				if discard.get_node("Area2D") in self.get_node("Area2D").get_overlapping_areas():
					to_zone(discard)
					return

				if parent.is_in_group("cardspaces"):
					to_zone(parent)
					return

				#check overlap with card spaces
				for area in $Area2D.get_overlapping_areas():
					if area.get_parent().is_in_group("cardspaces") and (area.get_parent().get_card() == null):
						emit_signal("played")
						to_zone(area.get_parent())
						return

			if parent in [hand, walk]:
				dragging = false
				small_rotation()
				return


		elif event is InputEventScreenTouch:
			if event.pressed and event.get_index() == 0:
				var touchpos = event.get_position()
				self.move(touchpos)

func set_landscape():
	# rotate from portrait to landscape
	var newsize = Vector2($Control.rect_size.y, $Control.rect_size.x)
	$Control.rect_size = newsize
	$Control.rect_position = $Control.rect_size * -0.5
	$Highlight.rotate(PI/2)
	is_draggable = false

func set_texture(texturepath: String):
	$Sprite.texture = load(texturepath)

func face_down():
	$Durability.visible = false
	$Sprite.visible = false
	$Back.visible = true
	is_draggable = false
	$Name.visible = false
	$Description.visible = false

func face_up():
	$Durability.visible = true
	$Sprite.visible = true
	$Back.visible = false
	is_draggable = true
	$Name.visible = true
	$Description.visible = true

func _ready():
	var timeDict = OS.get_time()
	#seed((1 + timeDict.hour) * (1 + timeDict.minute) * (1 + timeDict.second))
	randomize()
	self.add_to_group("cards")
	small_rotation()
	if get_parent() == deck:
		face_down()

func _on_Control_mouse_entered():
	if self.is_draggable:
		$Highlight.visible = true

func _on_Control_mouse_exited():
	$Highlight.visible = false

func activate_landscape(card):
	emit_signal("activate_landscape", card)
