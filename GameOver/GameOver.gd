extends CanvasLayer

export var max_name_length = 32

onready var root = get_tree().root.get_child(0)
onready var opponent = root.get_node("Opponent")
var myname = ""

func _on_NameInput_text_changed():
	if 0 < len($NameInput.text) and $NameInput.text[-1] == '\n':
		$NameInput.text = $NameInput.text.rstrip("\n")
		print("submitting score!")
		myname = $NameInput.text
		
		var score = opponent.turncounter
		$HTTPRequest.request(
			"https://leaderboard.pieceof.sh/",
			["Content-Type: application/json"],
			true,
			HTTPClient.METHOD_POST,
			to_json({
				"dear hackerman": "Congratulations! you found our shitty API. Yep. It's super insecure, and you can pretty easily just set whatever score. You could hack it, and make other people feel bad - or you could just feel good about yourself right now, and let the nice people play fair without messing it up. Up to you.",
				"name": $NameInput.text,
				"score": score
			})
		)

func pad_to_maxsize(name):
	var result = name.left(max_name_length)
	while len(result) < max_name_length:
		result += " "
	return result

func format_top_10(leaderboard):
	var count = 1
	var result = ""
	for line in leaderboard:
		var name = pad_to_maxsize(line[0])
		var score = str(line[1])
		result += str(count) + ". "
		if count < 10:
			result += " "
		result += name
		result += " "
		result += score
		result += "\n"
		count += 1
		if count > 10:
			break
			
	var my_placement = find_my_placement(leaderboard)
	if my_placement > 1:
		result += "...\n"
		result += str(my_placement) + ". "
		result += pad_to_maxsize(myname)
		result += str(opponent.turncounter)
	return result

func find_my_placement(leaderboard):
	var count = 0
	for line in leaderboard:
		count += 1
		if line[0] == myname:
			return count

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	# just assume everything went well and crash if shit sucks lol

	# hide input stuff
	$NameInput.visible = false
	$NameLabel.visible = false
	
	# set leaderboard
	var leaderboard = JSON.parse(body.get_string_from_utf8()).result
	var leaderboard_text = format_top_10(leaderboard)
	$Leaderboard.text = leaderboard_text
	$Leaderboard.visible = true
