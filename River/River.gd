extends Node

onready var parent = get_parent()

func _ready():
	yield(parent, "ready")
	parent.set_landscape()
	parent.add_to_group("landscapes")
	parent.set_texture("res://assets/river.png")
	parent.has_water = true
	parent.has_trees = true
	parent.has_game = false
