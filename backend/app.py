from pathlib import Path
from fastapi import FastAPI
from pydantic.dataclasses import dataclass
from dirdict import DirDict

app = FastAPI()
ddict = DirDict("/data")


@dataclass
class ScoreSubmission:
    name: str
    score: int


@app.post("/")
def post(data: ScoreSubmission):

    if data.name in ddict:
        previous_score = int(ddict[data.name].decode())
        if data.score <= previous_score:
            # don't update score
            return view()

    ddict[data.name] = str(data.score).encode()
    return view()


@app.get("/")
def view():
    scores = [[name, int(score.decode())] for name, score in ddict.items()]
    return list(sorted(scores, key=lambda d: d[1], reverse=True))
