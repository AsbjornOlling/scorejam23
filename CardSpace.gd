extends Node2D

signal done_activating

export var active_offset = 20

func activate_landscape(card):
	yield(get_tree().create_timer(0.5), "timeout")
	for node in get_children():
		if node.is_in_group("cards"):
			# move card up
			node.move(
				Vector2(
					node.global_position.x,
					node.global_position.y - active_offset
				),
				0.3,
				Tween.TRANS_EXPO
			)
			yield(node.get_node("Tween"), "tween_all_completed")
			
			# activate
			node.activate_landscape(card)
			yield(get_tree().create_timer(1), "timeout")
			
			# move back down
			node.move(
				Vector2(
					node.global_position.x,
					node.global_position.y + active_offset
				),
				0.3,
				Tween.TRANS_EXPO
			)
			yield(node.get_node("Tween"), "tween_all_completed")
			
	emit_signal("done_activating")
	print("done activating one cardspace")


func get_card():
	for node in get_children():
		if node.is_in_group("cards"):
			return node
	return null
