extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var root = get_tree().root.get_child(0)
onready var food = root.get_node("Food")
onready var parent = get_parent()



# Called when the node enters the scene tree for the first time.
func _ready():
	yield(parent, "ready")
	parent.set_texture("res://assets/fish.png")
	parent.to_zone(food)
	
	parent.get_node("Name").text = "FISH"
	parent.get_node("Description").text = "FOOD"


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
