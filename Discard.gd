extends Node2D


onready var root = get_tree().root.get_child(0)
onready var deck = root.get_node("Deck")

signal shuffle_into_deck_done

func shuffle_into_deck():
	for child in self.get_children():
		if child.is_in_group("cards"):
			child.to_zone(deck)
			yield(child.get_node("Tween"), "tween_all_completed")
	deck.shuffle()
	emit_signal("shuffle_into_deck_done")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if Input.is_action_just_pressed("testinput"):
		print("Hello there")
	if Input.is_action_just_pressed("discard_to_deck"):
		shuffle_into_deck()

