extends Node

onready var root = get_tree().root.get_node("World")
onready var opponent = root.get_node("Opponent")
onready var discard = root.get_node("Discard")
onready var hand = root.get_node("Hand")
onready var food = root.get_node("Food")

var rabbitscene = preload("res://Rabbit/Rabbit.tscn")

func _ready():
	yield(get_parent(), "ready")
	get_parent().set_durability(4)
	get_parent().set_texture("res://assets/bow.png")
	get_parent().connect("activate_landscape", self, "_on_landscape")
	get_parent().get_node("Name").text = "BOW"
	get_parent().get_node("Description").text = "USE ON\nPLAINS"

func _on_landscape(card):
	# check if card is in play
	if not get_parent().is_in_play():
		return # do nothing

	# add rabbit to food pile if card has game
	if card.has_game:
		var rabbit = rabbitscene.instance()
		rabbit.z_index = get_parent().z_index - 1
		rabbit.global_position = get_parent().global_position
		self.add_child(rabbit)
		
		#send to food pile
		rabbit.to_zone(food)
		yield(get_tree().create_timer(1), "timeout")
		
	
	get_parent().decrement_durability()
	
	# send self to discard pile
	#get_parent().to_zone(discard)
