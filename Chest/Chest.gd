extends Node

onready var world = get_tree().root.get_node("World")
onready var chestdialogscene = preload("res://ChestDialog/ChestDialog.tscn")
onready var bowscene = preload("res://Bow/Bow.tscn")
onready var fishingrodscene = preload("res://FishingRod/FishingRod.tscn")
onready var shovelscene = preload("res://Shovel/Shovel.tscn")
onready var hammerscene = preload("res://Hammer/Hammer.tscn")
onready var possible_scenes = [bowscene, bowscene, fishingrodscene, fishingrodscene, hammerscene, shovelscene]


func _ready():
	yield(get_parent(), "ready")
	get_parent().set_landscape()
	get_parent().add_to_group("events")
	get_parent().set_texture("res://assets/chest.png")
	get_parent().connect("activate_event", self, "_on_activate_event")

func _on_activate_event():
	# instance chestdialog on world
	var dialog = chestdialogscene.instance()
	var choice1 = randi() % len(possible_scenes)
	dialog.set_left_option(possible_scenes[choice1].instance())
	var choice2 = randi() % len(possible_scenes)
	
	while possible_scenes[choice1] == possible_scenes[choice2]:
		choice2 = randi() % len(possible_scenes)
	
	dialog.set_right_option(possible_scenes[choice2].instance())
	world.add_child(dialog)
	yield(dialog, "dialog_done")
	get_parent().emit_signal("event_done")
