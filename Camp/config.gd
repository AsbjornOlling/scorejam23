extends Node

onready var parent = get_parent()
onready var root = get_tree().root.get_child(0)
onready var deck = root.get_node("Deck")
onready var opponent = root.get_node("Opponent")
onready var hand = root.get_node("Hand")
onready var walk = root.get_node("Walk")
onready var food = root.get_node("Food")


func _ready():
	yield(parent, "ready")
	parent.set_texture("res://assets/walk.png")
	parent.connect("played", self, "_on_play")

func _on_play():
	if food.get_children():
		food.remove_child(food.get_child(len(food.get_children()) - 1))
		#TODO: remove nicely
		
		yield(get_tree().create_timer(1), "timeout")
		parent.to_zone(walk)
		yield(parent.get_node("Tween"), "tween_all_completed")
		yield(get_tree().create_timer(1), "timeout")
		
		deck.deal_cards(max(3 - len(hand.get_children()), 0))
		#yield(deck, "deal_done")
	else:
		parent.to_zone(walk)
