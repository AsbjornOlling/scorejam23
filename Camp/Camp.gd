extends Node

onready var parent = get_parent()
onready var root = get_tree().root.get_child(0)
onready var deck = root.get_node("Deck")
onready var opponent = root.get_node("Opponent")
onready var hand = root.get_node("Hand")
onready var walk = root.get_node("Walk")
onready var food = root.get_node("Food")


func _ready():
	yield(parent, "ready")
	parent.set_texture("res://assets/camp.png")
	parent.connect("played", self, "_on_play")
	
	parent.get_node("Name").text = "CAMP"
	parent.get_node("Description").text = "USES\nFOOD"

func _on_play():
	root.get_node("InputBlocker").visible = true
	if food.get_children():
		# move top foodcard up out of screen
		var foodcard = food.get_children()[-1]
		foodcard.move(
			Vector2(food.global_position.x, -200),
			1,
			Tween.TRANS_EXPO
		)
		# kill food card on arrival
		foodcard.get_node("Tween").connect("tween_all_completed", foodcard, "queue_free")
		# wait for food card move
		yield(foodcard.get_node("Tween"), "tween_all_completed")

		# move to walk zone
		parent.to_zone(walk)
		yield(parent.get_node("Tween"), "tween_all_completed")
		yield(get_tree().create_timer(1), "timeout")

		# activate cards without stepping forward
		opponent.activate_cards()

		var draw_cards = max(4 - len(hand.get_children()), 0)
		if draw_cards:
			deck.deal_cards(draw_cards)
			yield(deck, "deal_done")
	else:
		parent.to_zone(walk)
		yield(parent.get_node("Tween"), "tween_all_completed")
		root.game_over()
	root.get_node("InputBlocker").visible = false

