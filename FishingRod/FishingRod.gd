extends Node

var fishscene = preload("res://Fish/Fish.tscn")

onready var root = get_tree().root.get_node("World")
onready var foodzone = root.get_node("Food")

func _ready():
	yield(get_parent(), "ready")
	var parent = get_parent()
	parent.set_durability(4)
	parent.set_texture("res://assets/fishing_rod.png")
	parent.connect("activate_landscape", self, "_on_landscape")
	get_parent().get_node("Name").text = "FISHING\nROD"
	get_parent().get_node("Description").text = "\nUSE ON\nWATER"

func _on_landscape(card):
	
	if not get_parent().is_in_play():
		return # do nothing
	
	# TODO: decrement durability
	
	# add fish to food pile
	if card.has_water:
		var fish = fishscene.instance()
		fish.z_index = get_parent().z_index - 1
		fish.global_position = get_parent().global_position
		self.add_child(fish)
		
		fish.to_zone(foodzone)
		yield(get_tree().create_timer(1), "timeout")
	
	get_parent().decrement_durability()
