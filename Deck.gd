extends Node2D

onready var root = get_tree().root.get_child(0)
onready var hand = root.get_node("Hand")
onready var discard = root.get_node("Discard")

signal deal_done

func deal_cards(n):
	for i in range(n):
		if self.get_children():
			#draw a card
			var top_card = self.get_child(0)
			top_card.to_zone(hand)
			yield(top_card.get_node("Tween"), "tween_all_completed")
			
		elif len(discard.get_children())>1:
			#shuffling discard pile into deck
			discard.shuffle_into_deck()
			yield(discard, "shuffle_into_deck_done")
			yield(get_tree().create_timer(0.5), "timeout")
			
			# draw a card
			var top_card = self.get_child(0)
			top_card.to_zone(hand)
			yield(top_card.get_node("Tween"), "tween_all_completed")
		else:
			#no more cards
			pass

	# XXX: careful! this timer fixes a race condition
	yield(get_tree().create_timer(0.5), "timeout")
	emit_signal("deal_done")

func shuffle():
	#chooses 100 pairs of source- and destination-points and move accordingly
	if self.get_children():
		for i in range(100):
			var index1 = randi() % len(self.get_children())
			var index2 = randi() % len(self.get_children())
			self.move_child(self.get_child(index1), index2)

func _process(delta):
	if Input.is_action_just_pressed("draw_card"):
		deal_cards(1)
	if Input.is_action_just_pressed("shuffle"):
		shuffle()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

